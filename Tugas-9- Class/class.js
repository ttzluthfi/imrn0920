console.log(`====== pembatas =====`)
console.log (` Nomer 1 (Release 0 and Release 1)`)
class Animal {
    constructor (name){
        this.peliharaname = name;
        this.legs = 4;
        this.cold_blood = false;
    
    }
        get name () {
            return this.peliharaname;
        }

}

var sheep = new Animal("shaun");

console.log(sheep.name)// "sahun"
console.log(sheep.legs)// 4
console.log(sheep.cold_blood)


// Code class Ape dan class Frog di sini
 class Ape extends Animal {
     constructor(name){
     super (name);
     this.legs = 2;
     this.cold_blood = false;

     }
     yell(){
     return "Auooo"
     }
 }

 class Frog extends Animal{
     constructor(name) {
        super (name);
        this.legs = 4;
        this.cold_blood = false;
     }
     jump(){
         return "hop hop"
     }
 }
 console.log(`batas ----`)
var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.yell()) // "Auooo"
console.log(`batas ----`)
var kodok = new Frog("buduk")
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.jump()) // "hop hop" 


console.log(`====== pembatas =====`)
console.log (` Nomer 2 (function to class)`)
/*function Clock({ template}) {
    var timer;

    function render () {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours ;


        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins ;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs ;

        var output = template 
            .replace ('h', hours)
            .replace('m', mins)
            .replace('s', secs);

            console.log(output);
    }

    this.stop = function() {
        clearInterval(timer);
    };

    this.start = function() {
        render();
        timer = setInterval(render , 10000)
    };
}

    var clock = new Clock ({template : 'h:m:s'});
    clock.start(); 
*/


    class Clock {
        // Code di sini
        constructor ({ template }){
            this.template =  template;
        }

        render () {
            let  date = new Date();
            let hours = date.getHours();
            if (hours < 10) hours = '0' + hours ;
    
    
            let mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins ;
    
            let secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs ;
    
            let output = this.template 
                .replace ('h', hours)
                .replace('m', mins)
                .replace('s', secs);
    
                console.log(output);
        }

        stop () {
            clearInterval(this.timer);
        }
        start ( ) {
        this.render();
        this.timer = setInterval(() => this.render() , 10000)
        }
    }
    
    var clock = new Clock({template: 'h:m:s'});
    clock.start();
    