//deklarasi function tanpa return
function hello () {
    console.log("hello world !");
    
}
//deklarasi mengunakan variabel tanpa return
var hello2 = function () {
    console.log("Hello world yang kedua ")
}


//jalakan function
hello()
hello2()


// deklarasi function dengan return 
function greet() {
    var String = "halo dengan "
    return String;
}
//parameter 
function greet2(greeter) {
    var String = "halo dengan " + greeter + " disini"
    return String;
}
//funciton tambahan 
function greet3(greeter, time) {
    var string = "Halo selamat "+ time + " ! , "+ "dengan "+ greeter +" di sini!"
    return string;
}

var sapa = greet2("hai")
var sapa2 = greet2("haiss")
var sapa3 = greet3("jane ", "Malam")

console.log(sapa)
console.log(sapa2)
console.log(sapa3)
// ngeprint function langsung  
console.log(hello())
console.log(greet())

function kalikanDua(angka) {
    return angka * 2
  }
   
  var tampung = kalikanDua(2);
  console.log(tampung) 


  function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama + angkaKedua
  }
   
  console.log(tampilkanAngka(5, 3))