/// contoh 1
var hobbies = ["coding" , "cycling", "game" , "Music"]
console.log(hobbies)
console.log(hobbies.length)

console.log(hobbies[0])
console.log(hobbies[3])
console.log(hobbies[hobbies.length -1])


console.log("=== pembatas ====")
var number = [1, 2, 3, 4, 5]
var strings = ["a", "i", "u", "e", "o"]
var array2 = [1, "Ariq", "Male", 21]

console.log(number.length)//5
console.log(number[2])//
console.log(strings[2])

var lastIndexArray2 = array2.length - 1

console.log(number[number.length - 1])//5
console.log(array2[lastIndexArray2])
console.log("===pembatas====")


// metode array 
// 1 . push
var line = ["andi", "ika", "udin"]
//windi masuk anteran sama zaki dan upil
line.push("windi", "Zaki", "Upil")
console.log(line, "setelah dipush")
// 2. popline.pop
line.pop()
console.log(line, "stelah dipop")

console.log("==== pembatas ====")

var array = [];

function range (arr){  
    var lower = Math.min(arr[0],arr[1]);    
    var upper = Math.max(arr[0],arr[1]);  

    for (var i=lower;i<=upper;i++){  
        array.push(i);  
    }
    return array; // return the array to be used in the sum function
}  

function sum(array){  
    var total = 0; // need to create a variable outside the loop scope
    for(var i in array){  
       total = total+array[i];  
    }
    return total;
}  

console.log(sum(range([1,10]))); 

console.log("=== pembatas ====")

