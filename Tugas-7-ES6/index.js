//let and Const
/*
var x = 1;
if (x === 1) {
    var x = 2;
    console.log(x);
}
console.log(x);
*/


// es6
let x = 1;

if (x === 1) {
    let x = 2;

    console.log(x);
}

console.log(x);
console.log("=================================")
//const number = 42;
//number = 100; 


// arrow functions (Normal javascript)

function appFunction (){

}
appFunction();

//ES6
appFunction = () => {
    // isi function
}

appFunction();


//Default Parameters

function multiply(a, b=1 ) {return a * b;
}

console.log (multiply(5,2));
console.log(multiply(5));
console.log("=================================")
// expected output: 5


// Destructuring (Normal Javascripts)
/*
let stundentName = {
    firstName : 'peter',
    lastname : 'parker'


};

const firstName = stundentName.firstName;
const lastname = stundentName.lastname;
*/
// Destructuring (ES6)
console.log("=================================")
let stundentName = {
    firstName : 'peter',
    lastname : 'parker'


};
const {firstName, lastname} = stundentName;

console.log(firstName);
console.log(lastname);

//Rest Parameters + Spread Operator 
// Rest parameters
console.log("=================================")
let scores = ['98', '95', '93', '90', '87', '85']
let [first, second, third, ... restofScores] = scores;

console.log(first)
console.log(second)
console.log(third)
console.log(restofScores)

console.log("=================================")
let array1 = ['one', 'two']
let array2 = ['there', 'four']
let array3 = ['five', 'six']

// ES5 way // Normal javascript
/*

var combineArray = array1.concat(array2).concat(array3)
console.log(combineArray)
*/
// ES6 way
let combineArray = [...array1, ...array2, ...array3]
console.log(combineArray)

console.log("=================================")

//Enhanced object literals
/*const fullName = 'John Doe'

const john = {
    fullName : fullName}
    */

   const fullName = 'John Doe'
 
   const john = {fullName}


   //Template Literals
   const firstName = 'zell'
   const lastName = 'Liew'
   const teamName = 'unaffiliated'

   const theString = '${firstName} ${lastName}, ${teamName}'

   console.log(theString)