console.log(" Nomer 1 (Mengubah fungsi menjadi fungsi Arrow)")
console.log ("==============pembatas====================")
/// const golden = function goldenFunction () jadi const golden
 const golden = () => {
      console.log("this is golden!!")

  }
  golden();
console.log ("==============pembatas====================")
console.log(" Nomer 2 (Mensederhanakan menjadi object literal di ES6)")
console.log ("==================================")
/*const newFunction = function literal (firstName ,lastName){
    return {
        firstName : firstName,
        lastName : lastName,
        fullName : function() {
            console.log(firstName+ " " + lastName)
            return
        }
    }
}

newFunction("Wiliam", "imoh").fullName()
*/

const newFunction = (firstName, lastName) =>{
 return{
        firstName : firstName,
        lastName : lastName,
        fullName : function() {
            console.log(firstName+ " " + lastName)
            return
}
}
}
console.log ("==============pembatas====================")
console.log(" Nomer 3 (Destructuring)")
console.log ("==================================")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };
  const {firstName, lastName, destination, occupation} = newObject;
  console.log(firstName, lastName, destination, occupation)
console.log ("==============pembatas====================")
console.log(" Nomer 4 (Array Spreading)")
console.log ("==================================")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

console.log ("==============pembatas====================")
console.log(" Nomer 5 (Template Literals)")
console.log ("==================================")
const planet = "earth"
const view = "glass"

const before = ` Lorem  ${view} dolor sit amet, `+  
`consectetur adipiscing elit, ${planet}`+ `do eiusmod tempor ' +
'incididunt ut labore et dolore magna aliqua. Ut enim' +
' ad minim veniam`
// Driver Code
console.log(before) 