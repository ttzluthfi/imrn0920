import { StatusBar } from 'expo-status-bar';
import React from 'react';


import {NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SignIn , CreateAccount , Profile , Home , Search, Details, Search2, Splash} from './Screens';
import {createDrawerNavigator } from '@react-navigation/drawer'
import {AuthContext, authContext} from './Context'
import { StyleSheet,
   Text, 
   View ,
   TextInput,
   ScrollView,
   TouchableOpacity
  } from 'react-native';

  const AuthStack = createStackNavigator();
  const AuthStackScreen = () => {
    <AuthStack.Navigator>
    <AuthStack.Screen name="SignIn" component={SignIn} options={{ title: 'Sign in'}} />
    <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title : 'CreateAccount'}}/>
</AuthStack.Navigator> 
  }
  const Tabs = createBottomTabNavigator();
  const HomeStack = createStackNavigator();
  const ProfileStack = createStackNavigator();
  const SearchStack = createStackNavigator();

  const HomeStackScreen = () => {
      return(
      <HomeStack.Navigator>
          <HomeStack.Screen name="Home" component={Home}/>
          <HomeStack.Screen name="Details" component={Details} options={({route}) => ({
          title : route.params.name
          })}/>
      </HomeStack.Navigator>
      )
  }


  const ProfileStackScreen = () => {
      return(
      <ProfileStack.Navigator>
          <ProfileStack.Screen name="Profile"  component={Profile}/>
      </ProfileStack.Navigator>
      )
  }

  const SearchStackScreen = () => {
    return(
    <SearchStack.Navigator>
        <SearchStack.Screen name="Search"  component={Search}/>
        <SearchStack.Screen name="Search2"  component={Search2}/>
    </SearchStack.Navigator>
    )
}

const TabsScreen = () => {
    return (
        <Tabs.Navigator>
        <Tabs.Screen name="Home" component={HomeStackScreen} />
        <Tabs.Screen name="Profile" component={ProfileStackScreen} />
        <Tabs.Screen name="Search" component={SearchStackScreen} />
    </Tabs.Navigator>
    )
}

const Drawer = createDrawerNavigator();
const DrawerScreen = () => {
    return (
    <Drawer.Navigator initialRouteName="Profile">
    <Drawer.Screen name="Home" component={TabsScreen} />
<Drawer.Screen name="Profile" component ={ProfileStackScreen}/>
</Drawer.Navigator>
    )}

const RootStact = createStackNavigator();
const RootStactScreen = (userToken ) => {
    return(
    <RootStact.Navigator headerMode="none">
        {userToken ? (
        <RootStact.Screen name="App" component = {DrawerScreen} options={{
            animationEnable : false
        }} />
        ) : (
        <RootStact.Screen name="Auth" component = {AuthStackScreen} options={{
            animationEnable : false
        }}/>
        )}
        </RootStact.Navigator>
        
    )}

export default () => {
    const [isLoading, setIsLoading] = React.useState(true);

    const [userToken, setUserToken] = React.useState(null);


    const authContext = React.useMemo(() => {
        return{
            signIn : () => {
                    setIsLoading(false);
                    setUserToken('asfd');
            },
            signUp : () => {
                setIsLoading(false);
                setUserToken('asfd');
            },
                signOut : () => {
                    setIsLoading(false);
                    setUserToken(null);
                }
            }
        
        }, [])

    React.useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 1000)
    }, [] );

    if (isLoading) {
        return <Splash />
    }


    return (
        <AuthContext.Provider value={authContext}>
    <NavigationContainer>
            <RootStactScreen userToken={userToken}/>
    </NavigationContainer>
    </AuthContext.Provider>
    ) 
};


