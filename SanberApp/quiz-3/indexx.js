
import React from "react";
import {StyleSheet} from 'react-native';
import {NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Splash from './component/Screen/Splash';
import Home from './component/Screen/Home';
import Sign_in from './component/Screen/Sign_in';
import Signup from './component/Screen/Sign_up';
import { SignUp } from "../../Tugas13/Tugas13/RegisterScreen";
const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
       {/* <RootStack.Screen name="Splash" component={Splash} /> 
    <RootStack.Screen name="Signup" component={Signup} /> 
    <RootStack.Screen name="Sign_in" component={Sign_in} />  */}
    <RootStack.Screen name="Home" component={Home} />
  </RootStack.Navigator>
);
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    );
  }
}
