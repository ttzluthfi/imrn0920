import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { ScrollView } from 'react-native-gesture-handler';
const Home = () => {
    return (
        <View style={styles.container}>
            <View style={styles.searchSetting}>  
                <View style={styles.searchBar}>
                <MaterialCommunityIcons
          name="magnify"
          size={16}
          color="#3c3c3c3c"
        />
                    <Text style={styles.textSearchBar}>Search Product</Text>
                    <View style={styles.line}/>
                    <MaterialCommunityIcons style={styles.camera}
          name="camera-outline"
          size={16}
          color="#3c3c3c3c"
        />
                </View>
                <MaterialCommunityIcons style={styles.lonceng}
          name="bell-outline"
          size={16}
          color="#3c3c3c3c"
        
        />
            </View>
            <Image source={require('../Asset/Image/Slider.png')} style={styles.homeImage}/>
            <View style={styles.category}>
                <View style={styles.categoryDetails}>
                <View style={styles.ss}>
        <MaterialCommunityIcons
          name="tshirt-crew"//checkbox-blank-circle
          size={30}
          color="#3c3c3c3c"
        />
        </View>
                <Text style={styles.logoText}>Man</Text>
                </View>
                <View style={styles.categoryDetails}>
                <MaterialCommunityIcons
           name="tshirt-crew"
           size={30}
           color="#3c3c3c3c"
        />
       
                <Text style={styles.logoText}>Woman</Text>
                </View>
                
                <View style={styles.categoryDetails}>
                <MaterialCommunityIcons
          name="tshirt-v"
          size={30}
          color="#3c3c3c3c"
        />
        
                    <Text style={styles.logoText}>Kids</Text>
                </View>
                <View style={styles.categoryDetails}>
                <MaterialCommunityIcons
           name="home"
           size={30}
           color="#3c3c3c3c"
        />
                    <Text style={styles.logoText}>Home</Text>
                </View>
                <View style={styles.categoryDetails}>
                <MaterialCommunityIcons
           name="chevron-right"
           size={30}
           color="#3c3c3c3c"
        />
                    <Text style={styles.logoText}>More</Text>
                </View>
            </View >
            <View style={styles.container2}>
            <Text style={styles.Text}>Flash Sale</Text>
            <Text style={styles.jam}>03 : 30 : 30</Text>
            <Text style={styles.all}>all</Text>
            <MaterialCommunityIcons  style={styles.arrow}
           name="chevron-right"
           color="#F89C52"
        />
        </View>
        <View>
        <Image source={require('../Asset/Image/Sabun.png')} style={styles.sabun}/>
        <Image source={require('../Asset/Image/Radio.png')} style={styles.radio}/>
        <Image source={require('../Asset/Image/Google_Assitant.png')} style={styles.google}/>
        </View>
        <View style={styles.container2}>
        <Text style={styles.Text}>New Product</Text>
        <Text style={styles.all}>all</Text>
        <MaterialCommunityIcons  style={styles.arrow}
           name="chevron-right"
           color="#F89C52"
        />
        </View>
        <View>
        <Image source={require('../Asset/Image/Sepatu.png')} style={styles.sabun}/>
        <Image source={require('../Asset/Image/Kursi.png')} style={styles.radio}/>
        </View>
        
        <View>
            
        </View>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    Text : {
        marginRight : 180,
        marginTop : 10,
        fontSize : 40,
        fontWeight :"Bold",
    },
    sabun : {
        width:100, 
        height:100, 
        borderRadius:7,
        marginTop:15,
        marginRight : 250,
        marginBottom:10
    },
    google : {
        width:100, 
        height:100, 
        borderRadius:7,
        marginTop: -110,
        marginLeft : 300,
        marginBottom:10
    },
    radio : {
        width:100, 
        height:100, 
        borderRadius:7,
        marginTop: -110,
        marginLeft : 150,
        marginBottom:10
    },
    arrow : {
        marginLeft : 340,
        marginTop : -18,  
        fontSize : 16,
    },
    container2 : {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:14
    },
    jam : {
        marginLeft : 80,
        marginTop : -45,
        fontSize : 16,
        color : '#F89C52',
        
    },

    all : {
        marginLeft : 310,
        marginTop : -10,  
        fontSize : 16,
        
    },
    camera: {
        width:20, 
        height:16, 
        marginLeft:10
    },

    container: {
        flex:1,
        alignItems:'center',
        alignContent:'center',
        marginTop:35
    },
    
    category: {
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row'
    },
    categoryDetails: {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        marginHorizontal:14
    },
    homeImage: {
        width:365, 
        height:190, 
        borderRadius:7,
        marginTop:15,
        marginBottom:10
    },
    line:{
        height:16, 
        width:1, 
        backgroundColor:'#E6EAEE', 
        marginLeft:114, 
        marginRight:15
    },
    
    logoText: {
        color: '#616D80',
        fontSize:15
    },
    lonceng: {
        width:17,
        height:20, 
        marginLeft:12
    },
    searchBar: {
        width:335,
        height:44,
        borderRadius:11,
        borderWidth:1,
        borderColor:'#727C8E', flexDirection:'row',
        alignItems:'center',
        paddingLeft:15
    },
    searchLogo: {
        width:16, 
        height:16
    },
    searchSetting: {
        flexDirection:'row',
        alignContent:'center',
        alignItems:'center',
        justifyContent:'center'
    },
    textSearchBar: {
        fontSize:15, 
        color:'#727C8E', 
        marginLeft:20
    }
})
