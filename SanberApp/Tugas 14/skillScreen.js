import React, { Component } from "react";
import { Ionicons } from "@expo/vector-icons";
import data from './skillData.json'
import SkillDataku from './skillDataku'
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  FlatList
} from "react-native";

export default class LoginScreen extends Component {
    render() {

    return (

      <View style={styles.container}>
        <Image style={styles.logo} source={require("./Image/logo_putih.png")}  />
        <Text style={styles.deketsaber}> PORTOFOLIO </Text>
        
        <View style={styles.deketaccount}>
        <MaterialCommunityIcons
          name="checkbox-blank-circle"
          size={50}
          color="#3EC6FF"
        />
        </View>
        <View style={styles.account}>
        <MaterialCommunityIcons
          name="account"
          size={30}
          color="#fff"
        />
       </View>
       <Text style={styles.h1}>Hai , </Text>
        <Text style={styles.nama}>Ariq Luthfi Rifqi</Text>
        <Text style={styles.profesi}>SKILL</Text>
        <View style={styles.headerCard}>
        
          </View>
          <TouchableOpacity style={styles.libaryBtn}>
          <Text style={styles.loginText}>Libary / Framework</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.bahasaBtn}>
          <Text style={styles.loginText}>Bahasa Pemrogaman</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.teknologiBtn}>
          <Text style={styles.loginText}>Teknologi</Text>
        </TouchableOpacity>
        <View>
                <View>
                  
                </View>
                <View style={styles.hashtagFrame}/>
                    <View style={styles.hashtagFrame1}>
                       
                    </View>
                </View>
                <FlatList
                    data={data.items}
                    renderItem={(skill)=><SkillDataku skill={skill.item}/>}
                />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 41,
    // margin: 0,F
    // borderWidth: 1,
    // borderColor: "#003366",
    // borderRadius: 10,
    alignItems: "center",
    //justifyContent: "center",
  },
  logo: {
    marginTop: 5,
    marginLeft: 130,
     width: 198,
     height: 62,
  },
deketsaber : {
  marginTop : - 30,
  marginLeft : 230,
  fontWeight : 'bold',
  fontSize : 13,
  color : "#3EC6FF"
},
deketaccount: {
    marginTop : 10,
    position : 'relative',
    marginLeft : -290
  },
  account : {
    marginTop : -45,
    position : 'relative',
    marginLeft : -290
  },
profesi: {
    marginTop: 20,
    fontSize: 36,
    color: "#003366",
    marginLeft : -280
  },
  h1 : {
      marginTop : -35,
      position : 'relative',
    marginLeft : -180
  },
  nama : {
    marginTop : 1,
    position : 'relative',
  marginLeft : -100,
  fontSize : 16
  },
    headerCard: {
        width: 370,
        backgroundColor: "#3EC6FF",
        height: 5,
      },
     libaryBtn: {
        width: 130,
        backgroundColor: "#3EC6FF",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        height: 30,
        marginLeft : - 250,
        marginTop: 10,
        // marginBottom: 10,
      },
     bahasaBtn: {
        width: 140,
        backgroundColor: "#3EC6FF",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        height: 30,
        marginLeft : 35,
        marginTop: -30,
        
        // marginBottom: 10,
      },
      teknologiBtn: {
        width: 100,
        backgroundColor: "#3EC6FF",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        height: 30,
        marginLeft : 290,
        marginTop: -30,
        
        // marginBottom: 10,
      },
      loginText : {
          color : '#003366',
          fontWeight : 'bold',
      },
      kotak : {
          marginTop : 10,
        width: 370,
        height : 150,
        borderRadius: 10,
        backgroundColor: "#3EC6FF",
     
      },
      Scroll : {
        width: 370,
        height : 150,
      }
      

  
});
