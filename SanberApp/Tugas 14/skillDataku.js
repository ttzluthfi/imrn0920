import React, { Component } from 'react';
import { StyleSheet,Image, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class SkillDataku extends Component {
    render() {
        let skill = this.props.skill;

        return (
            <View style={styles.box}>
                <Icon name={skill.iconName} size={75} style={styles.iconName}/>
                <View style={styles.detailsView}>
                    <Text style={styles.skillName}>{skill.skillName}</Text>
                    <Text style={styles.categoryName}>{skill.categoryName}</Text>
                    <View style={styles.percentView}>
                        <Text style={styles.percentageProgress}>{skill.percentageProgress}</Text>
        
                    </View>
                </View>
                MaterialCommunityIcons name="arrow-right-bold"
          size={65}
          color="#3C3C3C3C"/>
                <View>
                  
                </View>
            </View>
        )
    }
}

export default SkillDataku;

const styles = StyleSheet.create({
    box: {
        width:350, 
        height:129,
        marginTop : 10,
        backgroundColor:'#B4E9FF', 
        alignSelf:'center', 
        flexDirection:'row',
        alignItems:'center', 
        paddingHorizontal:10, 
        borderRadius:8, 
        marginBottom:8, 
        elevation:8
    },
    iconName: {
        color: '#003366'
    },
    detailsView: {
        flexDirection:'column', 
        paddingLeft:22.5, 
        width:185, 
        height:120
    },
    skillName: {
        color:'#003366', 
        fontSize:24, 
        fontWeight:'bold'
    },
    categoryName: {
        fontSize:16, 
        fontWeight:'bold', 
        color:'#3EC6FF'
    },
    percentView: {
        alignItems:'flex-end'
    },
    percentageProgress: {
        fontSize:48, 
        fontWeight:'bold', 
        color:'white'
    },
    arrow: {
        width:22.5, 
        height:45, 
        marginLeft:29.75
    }
});
