import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class RegisterScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../Image/logo_putih.png")} />
        <Text style={styles.deketsaber}> PORTOFOLIO </Text>
        <Text style={styles.judul}>Register</Text>

        {/* < style={styles.body}> */}
        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Username</Text>
          <TextInput style={styles.inputText} />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Email</Text>
          <TextInput style={styles.inputText} />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Password</Text>
          <TextInput secureTextEntry style={styles.inputText} />
        </View>
        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Ulangi Password</Text>
          <TextInput secureTextEntry style={styles.inputText} />
        </View>
        <TouchableOpacity style={styles.daftarBtn}>
          <Text style={styles.loginText}>Daftar</Text>
        </TouchableOpacity>
        <Text style={styles.textAtau}>atau</Text>
        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>Masuk?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 41,

    alignItems: "center",
    
  },
  deketsaber : {
    marginTop : - 30,
    marginLeft : 180,
    fontWeight : 'bold',
    fontSize : 20,
    color : "#3EC6FF"
  },
  logo: {
    marginTop: 24,
    // width: 98,
    // height: 22,
  },
  judul: {
  
    marginTop: 30,
    marginBottom : 25,
    fontSize: 24,
    color: "#003366",
  },
  inputViewTitle: {
    fontSize: 16,
    alignItems: "flex-start",
    color: "#003366",
  },
  inputView: {
    width: "100%",
    height: 48,
    marginBottom: 20,
    marginTop: 10,
    justifyContent: "center",
  },
  inputText: {
    width : 300,
    height: 48,
    paddingStart: 10,
    borderColor: "#003366",
    borderWidth: 1,
  },
  loginText: {
    color: "white",
    fontSize: 24,
  },
  loginBtn: {
    width: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    marginTop: 10,
    // marginBottom: 10,
  },
  daftarBtn: {
    width: 140,
    backgroundColor: "#003366",
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    marginTop: 20,
    // marginBottom: 10,
  },
  textAtau: {
    marginTop: 10,
    fontSize: 24,
    color: "#3EC6FF",
    // marginBottom: 30,
  },
  body: {
    flex: 1,
  },
});
