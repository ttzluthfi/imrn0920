import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class LoginScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require("../Image/logo_putih.png")} />
        <Text style={styles.deketsaber}> PORTOFOLIO </Text>
        <Text style={styles.judul}> Login </Text>

        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Username / Email</Text>
          <TextInput style={styles.inputText} />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputViewTitle}>Password</Text>
          <TextInput secureTextEntry style={styles.inputText} />
        </View>
        <TouchableOpacity style={styles.loginBtn}>
          <Text style={styles.loginText}>Masuk</Text>
        </TouchableOpacity>
        <Text style={styles.textAtau}>atau</Text>
        <TouchableOpacity style={styles.daftarBtn}>
          <Text style={styles.loginText}>Daftar?</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 41,
    // margin: 0,F
    // borderWidth: 1,
    // borderColor: "#003366",
    // borderRadius: 10,
    alignItems: "center",
    //justifyContent: "center",
  },
  logo: {
    marginTop: 63,
    // width: 98,
    // height: 22,
  },
deketsaber : {
  marginTop : - 30,
  marginLeft : 180,
  fontWeight : 'bold',
  fontSize : 20,
  color : "#3EC6FF"
},
  judul: {
    marginTop: 40,
    marginBottom: 30,
    fontSize: 24,
    color: "#003366",
  },
  inputViewTitle: {
    fontSize: 16,
    alignItems: "flex-start",
    color: "#003366",
  },
  inputView: {
    width: 300,
    height: 48,
    marginBottom: 20,
    marginTop: 10,
    justifyContent: "center",
  },
  inputText: {
    height: 48,
    paddingStart: 10,
    borderColor: "#003366",
    borderWidth: 1,
  },
  loginText: {
    color: "white",
    fontSize: 24,
  },
  loginBtn: {
    width: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    marginTop: 10,
    // marginBottom: 10,
  },
  daftarBtn: {
    width: 140,
    backgroundColor: "#003366",
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    marginTop: 20,
    // marginBottom: 10,
  },
  textAtau: {
    marginTop: 10,
    fontSize: 24,
    color: "#3EC6FF",
    // marginBottom: 30,
  },
});
