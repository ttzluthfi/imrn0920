//tidak di berikan nama

class Car { 
    constructor (brand, factory) {
        this.brand = brand;
        this.factory = factory;
        this.sound = "Honk! Honk vroomvroo...."

    }
}

console.log(Car.name);

// diberikan nama
var car = class car2 {
    constructor (brand , factory){
        this.brand = brand
        this.factory = factory
    }
}

console.log(Car.name)


console.log("===== pembatas ======")

class Car3 {
    constructor (brand) {
        this.carname = brand;
    }

    present () {
        return " i have a "  + this.carname;
    }
}

mycar = new Car3("Ford")
console.log(mycar.present())


console.log("==== pembatas =====")

class Aku {
    constructor(brand, factory) {
        this.username = brand
        this.nim = factory

    }
    present(){
        return " my name " +this.username +" \n and my nim " +this.nim;
    }
}
myname = new Aku("Ariq Luthfi Rifqi", "E21182382")
console.log (myname.present())

console.log("=== pembatas ====")
class Cars {
    constructor(brand) {
        this.usercar = brand;
    }

    present(x) {
        return x + ", I have a " + this.usercar;
    }
}

mycar2 = new Cars("Alphard");
console.log(mycar2.present("mememk"))

console.log("==== pembatas ====")
class Alud {
    constructor ( brand ){
        this.carname = brand;
    }
    static hello () {
        return "Hello!1!"
    }
}

mycars = new Alud("aluds")
console.log(Alud.hello())

console.log("=== pembatas ====")

class Iols{
    constructor(brand) {
        this.carname = brand;
    }

    present() {
        return 'I have a ' + this.carname;
    }
}

class Model extends Iols {
    constructor(brand,mod){
        super(brand);
        this.model = mod;
    }
    show () {
        return this.present() + ', it is a ' + this.model;
    }
}

mycars = new Model("alpard", "mustang");
console.log(mycars.show());

console.log(`=== pembatas ====`)

class Pls {
    constructor(brand) {
        this.username = brand;

    }

    get cnam ( ) {
        return this.username;
    }
    set cnam (x) {
        return this.username = x;
    }
}

mycard = new Pls ("aku");
console.log(mycard.cnam)

console.log(`=== pembatas ====`)

class Ilos {
    constructor(brand ) {
        this._cardname = brand;
    }
    get carname () {
        return this._cardname;
    }
    set carname (x) {
        return this._cardname = x;
    }
}

mycar = new Ilos("alpahrd");
mycar.carname = "valve";
console.log(mycar.carname)
